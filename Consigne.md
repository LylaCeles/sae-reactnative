## A faire

- Gérer un Router fonctionnel de 4/5 pages // Avoir au moins 4/5 "Screen" d'applications en lien avec le Router
- Créer une interface en JSX/StyleSheet optimisée
- Mettre en place au moins un menu de Navigation (Header/Footer/Sidebar ...)
- Utiliser une librairie externe (Composant UX par exemple) et l'implémenter dans l'application
- Gérer les sauvegardes des datas en local sur le téléphone (historique score / settings )


#######################################################################################
## Fait !

- ✓ Utiliser deux composants natifs (ex : giroscope pour diriger)
- ✓ Avoir un dépôt versionné régulièrement avec des commits fonctionnels
- ✓ Gérer des composants réutilisables avec des PROPS
- ✓ Créer un composant de jeux autonome pouvant être chargé
- ✓ Utiliser la librarie asyncStorage



