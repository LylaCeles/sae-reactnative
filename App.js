import * as React from 'react';
import Main from './src/Main';
import { useState, useEffect } from 'react'; 
import * as Font from 'expo-font';

// import {io} from "socket.io-client" ;
// const socket = io("http://localhost:9000",{
//         autoConnect:false,
//         // rejectUnauthorized:false,
//         transports:["websocket"],

// });

function App() {
  const [fontsLoaded, setFontsLoaded] = useState(false);

  useEffect(() => {
    const loadFonts = async () => {
      await Font.loadAsync({
        'Varna-Bold': require('./assets/fonts/Varna-Bold.otf'),
        'Josefin Sans': require('./assets/fonts/JosefinSans-Regular.ttf'),
      });
      setFontsLoaded(true);
    };

    loadFonts();
  }, []);
  // useEffect(() => {
  //   console.log("coucou")
  //   // socket.connect();
  //   //   socket.emit("chat message", "voila un bo message");
  //   //   socket.on("chat message", (msg)=>{
  //   //       console.log(msg); 
  //   //   });   

  //   fetch("http://localhost:3000").then(
  //     function(res){
  //       if(res.ok){
  //         return res.json();
  //         //on recup en json
  //       }
  //     }
  //   ).then(function(data){
  //     console.log(data);
  //   });
  // }, []);

  if (fontsLoaded) {
    return (
      <Main />
    );
    
  }
}

export default App;