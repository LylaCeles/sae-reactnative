import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import GameZone from "../components/GameZone";
import { SafeAreaView } from "react-native-safe-area-context";
import { useState, useEffect } from 'react';
import ButtonLancage from "../components/ButtonLancage";
import De from "../components/ux/Dice";
import { useNavigation } from '@react-navigation/native';
import MainView from "./MainView";
import styles from "../Style";


export function GameZoneScreen({ route }) {
    const navigation = useNavigation();

    const { userName } = route.params;
    const [userPoint, setUserPoint] = useState(0);
    

    // useEffect(() => {
    //     console.log(selectChoice);
    // }, [selectChoice]);

    

    // function totalPoint() {
    //     if (selectChoice < 6) {
    //         let dataFormat = []
    //         dataDe.forEach(value => {
    //             dataFormat.push(value.value);
    //         });
    //         let counts = {};
    //         dataFormat.forEach(function (x) { counts[x] = (counts[x] || 0) + 1; });
    //         // console.log(counts["1"]);
    //         max = 0;
    //         for (let i = 1; i < 7; i++) {
    //             if (counts[i.toString()] != "undefined") {
    //                 if (i == selectChoice + 1) {

    //                     if (max < counts[i.toString()]) {
    //                         max = counts[i.toString()]
    //                     }
    //                 }
    //             }

    //         }
    //         // console.log(counts);
    //         // console.log("max :",max);
    //         // console.log("selectChoice :",selectChoice);
    //         return max * (selectChoice + 1);
    //     }
    //     else {
    //         // console.log(result, selectChoice);
    //         return result == selectChoice ? points[selectChoice] : 0;
    //     }
    // }

    // function afficheDes() {
    //     return dataDe.map((value) => {
    //         return (
    //             <De value={value} relanceDe={relanceDe} temoin={temoin} setTemoin={setTemoin} />
    //         )
    //     })
    // }

    return (
        // <SafeAreaView style={styles.contain}>
        // {endGame &&
        //         <View>
        //             {() => {
        //                 console.log("finito !!!");
        //                 navigation.navigate("GameZone", {
        //                     userPoint: userPoint,
        //                     userName:userName
        //                 })
        //             }}
        //         </View>
        //     } 
        <MainView >
        
            {/* {dataDe != null && cancelPopup != true ?
            // modale POPUP !!
                <View style={styles.containerPopup}>
                    <View style={styles.popup}>
                        {selectChoice == null ? <>
                            <Text>Vos dés :</Text>
                            <View style={styles.containDe}>
                                {afficheDes()}
                            </View>
                            <ButtonLancage lancageFunction={lancage} setCancelPopup={setCancelPopup} />
                        </>
                            :
                            <>
                                <TouchableOpacity style={styles.popupButton}
                                    onPress={() => {
                                        setSelectChoice(null);
                                        setCancelPopup(true);
                                    }}
                                >
                                    <Text>
                                        ×
                                    </Text>
                                </TouchableOpacity>
                                <Text>Votre choix : {figure[selectChoice]}</Text>
                                <Text>Vous gagnerez : {totalPoint()} points</Text>
                                <ButtonLancage setCancelPopup={setCancelPopup} setDataDe={setDataDe} setChoixUser={setChoixUser} choixUser={choixUser} selectChoice={selectChoice} setSelectChoice={setSelectChoice} setUserPoint={setUserPoint} userPoint={userPoint} points={totalPoint()} declareEndGame={declareEndGame} />
                            </>
                        }
                    </View>
                </View> : <></> */}
            {/* } */}
            <GameZone userName={userName} userPoint={userPoint} setUserPoint={setUserPoint}
            // lancageFunction={lancage} dataDe={dataDe} setSelectChoice={setSelectChoice} setCancelPopup={setCancelPopup} figure={figure} points={points} condition={condition} choixUser={choixUser} setResult={setResult} result={result} 
            />
        
        </MainView >
    );
}
