import { StyleSheet, View, Text, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import Header from '../components/Header';
import styles from '../Style';
import Footer from '../components/footer';
import { useNavigation } from '@react-navigation/native';
export default function MainView({ children }) {
    const navigation = useNavigation();
    const insets = useSafeAreaInsets();
    return <View style={{
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingTop: insets.top,
        paddingBottom: insets.bottom,
        paddingLeft: insets.left,
        paddingRight: insets.right,
    }}>
        <View style={styles.contain}>
        <Header/>
        {children}
        <Footer/>
        </View>
    </View>

}
