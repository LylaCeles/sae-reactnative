import { useSafeAreaInsets } from "react-native-safe-area-context";
import LoginZone from "../components/LoginZone";
import MainView from "./MainView";
import { StatusBar, View } from "react-native";

export function LoginScreen() {
    const insets = useSafeAreaInsets();
    return <View style={{
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingTop: insets.top,
        paddingBottom: insets.bottom,
        paddingLeft: insets.left,
        paddingRight: insets.right,
    }}><StatusBar barStyle="light-content" backgroundColor="#A83536"/>
        <LoginZone/>
        </View>
  }