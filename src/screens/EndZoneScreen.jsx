import { Button, Text, View, StyleSheet, TouchableOpacity } from "react-native";
import EndZone from "../components/EndZone";
import { SafeAreaView } from "react-native-safe-area-context";
import MainView from "./MainView";


export function EndZoneScreen({ route }) {
  const { userName, userPoint } = route.params;
    return (
      <MainView>
        <EndZone userName={userName} userPoint={userPoint}/>
      </MainView>
    );
  }


  const styles = StyleSheet.create({

    contain: {
        backgroundColor: "green",
        width: "100%",
        height: "85%",
    },
    buttonText: {
        margin: "15%",
        padding: 20,
        backgroundColor: 'red',

    }
});