import { Button, Text, View, StyleSheet, TouchableOpacity } from "react-native";
import HomeZone from "../components/HomeZone";
import { SafeAreaView } from "react-native-safe-area-context";
import MainView from "./MainView";


export function HomeScreen({route}) {
  const { userName } = route.params;
    return (
      // <SafeAreaView>
        <MainView>
        <HomeZone userName={userName}/>
        </MainView>
      // </SafeAreaView>
    );
  }