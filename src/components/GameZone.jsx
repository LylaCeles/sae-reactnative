import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import Footer from './footer';
import ListCombi from './ListCombi';
import Hasard from './Hasard';
import De from './ux/Dice';
import { useEffect, useState } from 'react';
import Header from './Header';
import MainView from '../screens/MainView';
import styles from "../Style";
import Dices from './Dices';
import ButtonLancage from './ButtonLancage';
import ChoiceModale from "./ChoiceModale";
import InformationModale from './InformationModale';
export default function GameZone({ userName, userPoint, setUserPoint
    // lancageFunction, dataDe, setSelectChoice, setCancelPopup, figure, points, condition, choixUser, setResult, result 
}) {
    const navigation = useNavigation();
    const [dataDe, setDataDe] = useState(null);
    const [temoin, setTemoin] = useState(false);
    const [relance, setRelance] = useState(null);
    const [cancelPopup, setCancelPopup] = useState(false);
    const [selectChoice, setSelectChoice] = useState(null);
    const [choixUser, setChoixUser] = useState([false, false, false, false, false, false, false, false, false, false, false, false]);
    const [result, setResult] = useState(null);
    // const [endGame, setEndGame] = useState(false);
    const [throwDice, setThrowDice] = useState(3);
    let figure = ["Multiple de 1", "Multiple de 2", "Multiple de 3", "Multiple de 4", "Multiple de 5", "Multiple de 6", "Brelan", "Carré", "Full", "Petite Suite", "Grande Suite", "Yam's"]
    let condition = ["La somme de tout les dés de 1", "La somme de tout les dés de 2", "La somme de tout les dés de 3", "La somme de tout les dés de 4", "La somme de tout les dés de 5", "La somme de tout les dés de 6", "Obtenir trois dés de même valeur", "Obtenir quatre dés de même valeur ", "Obtenir un brelan et deux dés de même valeur ", "Obtenir une suite croissante de quatre dés ", "Obtenir une suite croissante de cinq dés ", "Obtenir cinq dés de même face",]

    let points = ["Total de 1", "Total de 2", "Total de 3", "Total de 4", "Total de 5", "Total de 6", 18, 24, 25, 30, 40, 50];

    const [showModale, setShowModale]= useState(false);
    const [informationSelected, setInformationSelected]= useState(null);



    useEffect(() => {
        if (throwDice == 0) {
            stopThrow();
        }
    }, [throwDice])
    function selectRethrow(j) {
        relanceArray = []
        for (let i = 0; i < 5; i++) {
            if (j.index == i) {
                // value={j}
                relanceArray.push(j);
            }
            else {

                if (selectRethrow != null) {
                    relanceArray.push(dataDe[i]);
                }
                else {
                    relanceArray.push(relance[i]);
                }
            }
        }
        setRelance(relanceArray);
    }

    function lancage() {
        setThrowDice(throwDice - 1);

        if (relance == null) {

            let desArray = [];
            // let j=2;
            for (let i = 0; i < 5; i++) {
                desArray.push({
                    index: i,
                    relance: false,
                    value: Math.floor(Math.random() * 6) + 1,
                })
            }
            setDataDe(desArray);
        }
        else if (relance.length) {
            let desArray = relance;
            for (let i = 0; i < 5; i++) {
                if (desArray[i].relance == true) {
                    desArray[i].value = Math.floor(Math.random() * 6) + 1;
                }
            }
            setDataDe(desArray);
            setRelance(null);
            setTemoin(false);
        }


    }
    function stopThrow() {
        // console.log("blblbl stooop");
        setRelance(null);
        setThrowDice(0);
    }
    function declareEndGame() {
        if (!choixUser.includes(false)) {
            // setEndGame(true);
            console.log("endGame", choixUser);

            navigation.navigate("EndZone", {
                userPoint: userPoint,
                userName: userName
            })

        }
    }

    function confirmChoice(point) {
        setCancelPopup(true);
        choix = choixUser;
        choix[selectChoice] = true;
        console.log("liste combi avant changement", choix );
        setChoixUser(choix);
        setSelectChoice(null);
        setDataDe(null);
        setUserPoint(point + userPoint);
        setCancelPopup(false);
        setThrowDice(3);
        declareEndGame();
    }

    return (
        <>
            <InformationModale show={showModale} handleClose={()=>setShowModale(false)} figure={figure[informationSelected]} condition={condition[informationSelected]} value={points[informationSelected]}/>
            <ChoiceModale selectChoice={selectChoice} cancelPopup={cancelPopup} setCancelPopup={setCancelPopup} dataDe={dataDe} figure={figure} confirmChoice={confirmChoice} result={result} points={points} setShowModale={setShowModale} setInformationSelected={setInformationSelected}/>

            <View style={styles.container}>
 
                <View style={styles.nameContainer}>

                    <Text style={[styles.pseudo, styles.text, {fontWeight:900}]}>{userName}</Text><Text style={[styles.pseudo, styles.text, {marginBottom:10}]}> {userPoint} points</Text>
                </View>
                <ListCombi throwDice={throwDice} setSelectChoice={setSelectChoice} dataDe={dataDe} setCancel={setCancelPopup} figure={figure} points={points} condition={condition} choixUser={choixUser} result={result} setResult={setResult} setInformationSelected={setInformationSelected} setShowModale={setShowModale} />

                <Dices diceArray={dataDe} throwDice={throwDice} selectRethrow={selectRethrow} temoin={temoin} setTemoin={setTemoin} />
                <Hasard lancageFunction={lancage} dataDe={dataDe} stopThrow={stopThrow} throwDice={throwDice} setRelance={setRelance} />
            </View>
        </>
    )
}
