import { useNavigation } from "@react-navigation/native";
import { useEffect, useState } from "react";
import { Text, View } from "react-native";
import styles from "../Style";
import { TextInput } from "react-native-paper";
import { TouchableOpacity } from "react-native";

export default function LoginZone() {

    const [name, setName] = useState('');
    const navigation = useNavigation();

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            setName("");
            // getUser();
        });

        return unsubscribe; // Nettoyage
    }, [navigation]);

    return (
        <>
            <View style={[styles.container, styles.containerLogin]}>
                <View style={[styles.contentLogin]}>
                    <Text style={[styles.title, styles.titleLogin]}>Yam's online</Text>
                    <TextInput
                        style={styles.input}
                        onEndEditing={(e) => {
                            setName(e.nativeEvent.text);
                            // storeUser(); getUser();
                        }}
                        placeholder={name != '' ? name : "Ton pseudo"}
                        placeholderTextColor="#95aa95"
                        contentStyle={{ fontFamily: "Josefin Sans" }}
                    />
                    <TouchableOpacity
                        onPress={() => {
                            navigation.navigate("Home", {
                                userName: name
                            })
                        }}
                        disabled={name ? false : true}
                        style={[styles.throwButton, name?"":styles.buttonDisabled ]}
                    >
                        <Text style={styles.buttonText}>Se connecter</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </>
    )


}