import { StyleSheet, View, Text, TouchableOpacity, TextInput } from 'react-native';
import React, { useState, useEffect } from 'react';
import Footer from './footer';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useNavigation } from '@react-navigation/native';
import styles from '../Style';
// import { useNavigation } from '@react-navigation/native';
// import ListCombi from './ListCombi';

export default function EndZone({ userName, userPoint }) {
    // const [data, setData] = useState(null);
    const navigation = useNavigation();

    const getUser = async () => {
        try {
            let scoreArray = await AsyncStorage.getItem("score");
            return JSON.parse(scoreArray);
        } catch (error) {
            console.error(error)
        }
    }


    const storeUser = async () => {
        const value = [{
            "userName": userName,
            "point": userPoint
        }]
        try {

            getUser().then((res) => {
                let dataArray = res!=null ? res.concat(value) : value;
                console.log("resultat get: ",dataArray);
                AsyncStorage.setItem("score", JSON.stringify(dataArray));
                navigation.navigate("Home", {
                    userName: userName
                })
            }).catch((err) => {
                console.error("Erreur :", err);
            });

        } catch (error) {
            console.log(error);
        }
    };

    return (
        <>
            <View style={styles.container}>
                <View style={styles.endMsg}>
                    <Text style={[styles.pseudo, styles.text]}>Vous avez fini {userName}!</Text>
                    <Text style={[styles.pseudo, styles.text]}>Vous avez {userPoint} points</Text>
                </View>

                <TouchableOpacity
                    onPress={() => {
                        storeUser();
                    }}
                    style={styles.throwButton}
                >
                    <Text style={[styles.textThrowButton, styles.text]}>Enregistrer le score</Text>
                </TouchableOpacity>
            </View>
        </>
    )
}


// contain: {
//     display: "flex",
//     // justifyContent:"center",
//     alignItems: "center",
//     backgroundColor: "#555",
//     width: "100%",
//     height: "85%",
// },
// title: {
//     fontSize: 50,
// },
// endMsg: {
//     display: "flex",
//     alignItems: "center",
//     width: "100%",
//     height: "25%",
// },
// buttonText: {
//     margin: "15%",
//     padding: 0,
//     backgroundColor: 'red',

// },

