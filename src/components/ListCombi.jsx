import { StyleSheet, View, Text, TouchableOpacity, ScrollView, Alert } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import FooterGame from './footerGame';
import React, { useState, useEffect } from 'react';
import { DataTable, DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import styles from '../Style';
import InformationModale from './InformationModale';

export default function ListCombi({ dataDe = null, setSelectChoice, setCancel, figure = [], points = [], condition = [], choixUser, setResult, result, throwDice, setShowModale, setInformationSelected }) {

    function verifYam() {
        let temoin = true;
        // console.log(Array.isArray(dataDe));
        dataDe.forEach(value => {
            console.log("est egale a la premiere ?",value.value != dataDe[0].value)
            if (value.value != dataDe[0].value) {
                temoin = false;
            }
        });
        console.log("yams ?", temoin == true && dataDe[0].value)
        return temoin == true && dataDe[0].value;
    }

    function verifBrelanCarre(type) {
        temoin = false
        for (var i = 1; i < 7; i++) {
            var count = 0;
            if (temoin != true) {
                for (var j = 0; j < dataDe.length; j++) {
                    if (dataDe[j].value === i) {
                        // console.log("kikou",dataDe[j].value);
                        count++;
                    }
                }
                temoin = count == type ? true : false;
            }
        }

        // console.log(type, count);
        // return doublon()==type && type==3 ? true : doublon()==2 && type==4 ? true : false;
        return type == 4 && doublon() == 2 ? true : temoin;
    }
    function doublon() {
        let dataTable = [];
        dataDe.forEach(value => {
            dataTable.push(value.value);
        })
        return (new Set(dataTable)).size
    }

    function Suite(type) {
        // 4 pour la petit suite 
        // 5 pour la grande
        let dataTable = [];
        dataDe.forEach(value => {
            dataTable.push(value.value);
        })
        let double = doublon();
        dataTable = [...new Set(dataTable.sort())];
        // console.log("resultat suite",dataTable)
        // console.log(dataTable);
        if (dataTable.length >= 4) {
            for (let i = 0; i < dataTable.length-1; i++) {
                // console.log(dataTable[i] != dataTable[i - 1] + 1)
                if (!(dataTable[i]==dataTable[i+1]-1)) {
                    // console.log("pas une suite")
                    return false;
                }
                // if (!(dataTable[i]==dataTable[i+1]-1)){
                //     console.log(dataTable[i],"ensuit", dataTable[i+1])
                // }
            }
            console.log("suite de", type,"?",type == dataTable.length)
            return type == dataTable.length;
        }
        return false;
    }

    function Multiple() {
        let dataFormat = []
        dataDe.forEach(value => {
            dataFormat.push(value.value);
        });
        let dataTable = new Set(dataFormat);
        const duplicates = dataFormat.filter(item => {
            if (dataTable.has(item)) {
                dataTable.delete(item);
            } else {
                return item;
            }
        });
        let result = [];
        // duplicates = [...new Set(duplicates)]
        duplicates.forEach(value => {
            result.push(value - 1);
        })
        // console.log(result);
        return (result);
    }

    useEffect(() => {
        if (dataDe != null) {
            let resultThrow=[];
            // console.log(choixUser)
            if (verifYam() && !choixUser[11]) {
                console.log("yam !");
                resultThrow.push(11);
            }
            if (verifBrelanCarre(3) && doublon() == 3 && !choixUser[5]) {
                console.log("Brelan !");
                resultThrow.push(6);
            }
            if (verifBrelanCarre(3) && doublon() == 2 && !choixUser[7]) {
                console.log("Full !");
                resultThrow.push(8);

            }
            if (verifBrelanCarre(4) && !choixUser[6]) {
                // console.log(doublon());
                console.log("carré !");
                resultThrow.push(7);
            }
            if (Suite(4) && !choixUser[8]) {
                console.log("Petit suite !");
                resultThrow.push(9);

            }
            if (Suite(5) && !choixUser[9]) {
                console.log("Grande suite !");
                resultThrow.push(10);


            }
            resultThrow=resultThrow.concat(Multiple());
            console.log("resultat ?", resultThrow)
            setResult(resultThrow);
            // setResult((lastResult)=>{
            //     let newResult =[];
            //     if (lastResult){
            //         newResult=[...lastResult];

            //     } 
            //     newResult.push(Multiple())
            //     return newResult;
            // });
        }
    }, [dataDe]);


    function bgColor(i) {
        // console.log("value table",i)
        if (!choixUser[i]) {
            if (dataDe != null) {

                if (!Array.isArray(result)) {
                    if (i === result) {
                        return '#c3efb8';
                    }
                }
                else {
                    if (result.includes(i)) {
                        return '#c3efb8';
                    }
                    else {
                        return 'white';
                    }

                }
            }

        }
        else {
            return '#3D4F0D';
        }

    }
    function textColor(i) {
        if (!choixUser[i]) {
            return "#205060";
        }
        else {
            return "#999";
        }
    }

    function dataTable() {
        let print = [];

        for (let i = 0; i < 12; i++) {
            if (!choixUser[i]) {
                
                print.push(
                    {
                        table: <DataTable.Row style={[styles.row, { backgroundColor: bgColor(i) }]}>

                        {/* <DataTable.Cell>{ }</DataTable.Cell> */}
                        <DataTable.Cell>
                            <Text style={{color:textColor(i)}}>
                            {figure[i]}
                            </Text>
                            </DataTable.Cell>
                        <DataTable.Cell><Text style={[styles.points, {color:textColor(i)}]}>{points[i]}</Text></DataTable.Cell>
                    </DataTable.Row>,
                    value: i
                })
            }
        }
        return print.map((view) => {
            return (
                // console.log(view.value)
                <TouchableOpacity key={view.value} onPress={
                    () => {
                        
                        if (throwDice>0) {
                            // Alert.alert(condition[view.value])
                            setInformationSelected(view.value);
                            setShowModale(true);
                        }
                        else {
                            setSelectChoice(view.value);
                            setCancel(false);

                        }
                    }
                }
                    disabled={bgColor(view.value) == "#3D4F0D" && true}>
                    {view.table}
                </TouchableOpacity>
            )
        })
    }

    // const customTheme = {
    //     ...DefaultTheme,
    //     colors: {
    //       ...DefaultTheme.colors,
    //       surface: 'red', // Applique une couleur de fond par défaut pour les surfaces
    //     },
    //   };

    return (
        <>
            {/* <InformationModale show={showModale} handleClose={()=>setShowModale(false)} figure={figure[informationSelected]} condition={condition[informationSelected]} value={points[informationSelected]}/> */}
            <View style={styles.tableHeader}>
                <Text style={styles.text}>Combinaison</Text>
                <Text style={styles.text}>Points</Text>
            </View>
            <ScrollView style={styles.tableContainer}>
                <PaperProvider >
                    <DataTable style={styles.table}>
                        {/* <DataTable.Header style={styles.tableHeader}>
                    <DataTable.Title>Combinaison</DataTable.Title>
                    <DataTable.Title numeric>Points</DataTable.Title>
                </DataTable.Header> */}

                        {dataTable()}

                    </DataTable>
                </PaperProvider>
            </ScrollView>
        </>
    );
}


