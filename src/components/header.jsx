import { StyleSheet, View, Text, StatusBar } from 'react-native';
import styles from '../Style';

export default function Header() {
    return(
    <View style={styles.header}>
        <Text style={styles.title}>Yam's online</Text>
        <StatusBar barStyle="light-content" backgroundColor="#A83536"/>
    </View>
    )
}
