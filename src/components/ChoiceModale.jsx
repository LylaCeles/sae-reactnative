import { Text, TouchableOpacity, View } from "react-native";
import ButtonLancage from "./ButtonLancage";
import Modale from "./ux/Modale";
import styles from "../Style";
import { useEffect, useState } from "react";

export default function ChoiceModale({ selectChoice, cancelPopup, setCancelPopup, dataDe, figure, confirmChoice = false, result, points }) {
    const [newPoint, setNewPoint] = useState(0);

    useEffect(() => {
        setNewPoint(totalPoint());


    }, [selectChoice])
    function totalPoint() {
        if (selectChoice < 6) {
            let dataFormat = []
            dataDe && dataDe.forEach(value => {
                dataFormat.push(value.value);
            });
            let counts = {};
            dataFormat.forEach(function (x) { counts[x] = (counts[x] || 0) + 1; });
            // console.log(counts["1"]);
            max = 0;
            for (let i = 1; i < 7; i++) {
                if (counts[i.toString()] != "undefined") {
                    if (i == selectChoice + 1) {

                        if (max < counts[i.toString()]) {
                            max = counts[i.toString()]
                        }
                    }
                }

            }
            return max * (selectChoice + 1);
        }
        else {
            // console.log(result, selectChoice);
            console.log("point gagné", result, selectChoice, points[selectChoice], result.includes(selectChoice))
            return result.includes(selectChoice) ? points[selectChoice] : 0;
        }
    }

    return (
        <>
            <Modale handleClose={() => setCancelPopup(true)} show={selectChoice != null && !cancelPopup}>
                <View style={styles.choiceModale}>
                    <Text style={[styles.h2, styles.text]}>Score</Text>
                    <Text style={styles.text}>Votre choix : {figure[selectChoice]}</Text>
                    <Text style={styles.text}>Vous gagnerez : {newPoint} points</Text>
                    <TouchableOpacity style={styles.button} onPress={() => {
                        confirmChoice(newPoint);


                    }}>
                        <Text style={styles.buttonText}>
                            Garder
                        </Text>
                    </TouchableOpacity>
                </View>
            </Modale></>
    )

}