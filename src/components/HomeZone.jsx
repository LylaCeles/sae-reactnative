import { StyleSheet, View, Text, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import React, { useState, useEffect } from 'react';
import Footer from './footer';
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import ListCombi from './ListCombi';
import styles from "../Style";
// import Header from './Header';
// import MainView from '../screens/MainView';

export default function HomeZone({ userName }) {
    const navigation = useNavigation();
    // const [name, setName] = useState('');
    const [data, setData] = useState(null);


    const getUser = async () => {
        try {
            let scoreArray = JSON.parse(await AsyncStorage.getItem("score"));
            scoreArray.sort((a, b) => b["point"] - a["point"]);
            console.log("sorting score", scoreArray);
            setData(scoreArray);
            // console.log("les joli data : ",data[0]);
        } catch (error) {
            console.error(error)
        }
    }


    // const storeUser = async () => {
    //     const value = {
    //         userName: name,
    //         point: 75
    //     }
    //     try {
    //         // console.log("test ",test);
    //         getUser();
    //         // console.log(test, value[1]);
    //         res = [data, value];
    //         // res=test.concat(value);
    //         // console.log("dftrgfwdfgvqdfw: ", res);

    //         AsyncStorage.setItem("score", JSON.stringify(res));
    //         // console.log("aled ",result);
    //         // setData(res)

    //     } catch (error) {
    //         console.log(error);
    //     }
    // };

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            // console.log('La screen Home est affichée.');
            // setName("");
            getUser();
        });

        return unsubscribe; // Nettoyage
    }, [navigation]);


    return (
        <>
            <View style={styles.container}>
                <View style={styles.data}>
                    <Text style={[styles.dataTitle, styles.text]}>Score déjà obtenu :</Text>
                    <ScrollView>
                        {data != null && <>
                            {data.map((element, i) => {
                                // { console.log("element :", element) }
                                if (element != null) {

                                    if (element["userName"]) {

                                        return <Text key={i} style={[styles.dataText, styles.text]}>{element["userName"]} : {element["point"]} points</Text>
                                    }
                                }

                            })}
                        </>}
                    </ScrollView>
                </View>


                {/* <TextInput
                    style={styles.input}
                    onEndEditing={(e) => {
                        setName(e.nativeEvent.text);
                        // storeUser(); getUser();
                    }}
                    placeholder={name != '' ? name : "Ton pseudo"}
                /> */}
                <TouchableOpacity
                    onPress={() => {
                        navigation.navigate("EndZone", {
                            userName: userName
                        })
                    }}
                    style={[styles.throwButton]}
                >
                    <Text style={styles.textThrowButton}>Jouer</Text>
                </TouchableOpacity>
            </View>
            {/* <TouchableOpacity
                onPress={() => {
                    navigation.navigate("GameZone", {
                        userName: name
                    })
                }}
                disabled={name ? false : true}
                style={[styles.button, !name && styles.buttonDisabled]}
            >
                <Text style={styles.buttonText}>Go</Text>
            </TouchableOpacity> */}
        </>
    )
}

