import { StyleSheet, View, Text, ScrollView, TouchableOpacity, Alert } from 'react-native';


export default function FooterGame() {

    function oui(){
        let print=[];
        let figure =[1,2,3,4,5,6,"Brelan", "Carré","Full","Petite Suite", "Grande Suite","Yam's"]
        let condition =["Max de uns","Max de deux","Max de trois","Max de quatres","Max de cinqs","Max de 6","Obtenir trois dés de même valeur", "Obtenir quatre dés de même valeur ","Obtenir un brelan et deux dés de même valeur ","Obtenir une suite croissante de quatre dés ", "Obtenir une suite croissante de cinq dés ","Obtenir cinq dés de même valeur"]
        for (let i = 0; i < 12; i++) {
            print[i]=(<TouchableOpacity style={styles.card} onPress={()=>{Alert.alert(condition[i].toString())}}>
                    <Text style={i!=11?styles.text: styles.yams}>{figure[i]}</Text>
            </TouchableOpacity>)
            
        }
        return print.map((view)=>{
            return(
                view
            )
        })
    }

    return (

        <View style={styles.footer}>
            {/* <Text style={styles.text}>Goal</Text>

            <ScrollView horizontal={true}
                showsHorizontalScrollIndicator={true}>
                    {oui()}
            </ScrollView> */}
        </View>
    )
}

const styles = StyleSheet.create({

    footer: {
        backgroundColor: "black",
        width: "100%",
        height: "15%",
        
    },
    text: {
        textAlign:"center",
        justifySelf:"center",
        color: "white",
    },
    yams:{
        textAlign:"center",
        color:"white",
        fontSize:15,
        fontWeight:"900",
    },
    card: {
        justifyContent:"center",
        backgroundColor: "red",
        width: 50,
        height: 50,
        margin: 20
    }

});
