import { StyleSheet, View, Text, TouchableOpacity, Vibration, FlatList } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import FooterGame from './footerGame';
import React, { useState, useEffect } from 'react';
import styles from '../Style';
// import { FlatList } from 'react-native-web';


export default function Hasard({ lancageFunction, dataDe, throwDice, stopThrow = false }) {
    const navigation = useNavigation();
    const [temoin, setTemoin] = useState(false);


    function lancage() {
        lancageFunction();
        // setTemoin(true);
    }

    return (
        <View style={styles.buttonContainer}>
            {throwDice <= 3 && throwDice > 0 ? <>
                {throwDice < 3 && 
                <TouchableOpacity style={styles.throwButton}
                    onPress={() => stopThrow()}>
                    <Text style={styles.textThrowButton}>Garder</Text>
                </TouchableOpacity>
                }
                <TouchableOpacity
                    onPress={() => { lancage(); Vibration.vibrate(150); }}
                    // disabled={name ? false : true}
                    style={[styles.throwButton, throwDice >= 1 && styles.nextThrow]}
                >
                    <Text style={styles.textThrowButton}>Lancer ({throwDice})</Text>
                    {/* <View style={styles.buttonDice}>
                        
                        <View style={styles.groupPoint}>
                        <View style={styles.dice}></View>
                        <View style={styles.dice}></View>
                        </View>
                        <View style={styles.groupPoint}>
                        <View style={styles.dice}></View>
                        <View style={styles.dice}></View>
                        </View>
                        
                        </View> */}
                </TouchableOpacity>
                </>
                // </View>
                : <></>
            }
        </View>
    )
}

