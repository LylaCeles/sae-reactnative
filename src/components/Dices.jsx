import { View } from "react-native";
import styles from "../Style";
// import De from "./ux/Dice";
import Dice from "./ux/Dice";

export default function Dices({ diceArray, selectRethrow, temoin, setTemoin=false, throwDice }) {

    function afficheDes() {
        return diceArray.map((value,i) => {
            return (
                <Dice key={i} value={value} disabled={throwDice==0} selectRethrow={selectRethrow} temoin={temoin} setTemoin={setTemoin}/>

            )
        })
    }
    return (
        <View style={styles.des}>
            {diceArray != null ?
                afficheDes(diceArray)
                : <></>}
        </View>
        )

}