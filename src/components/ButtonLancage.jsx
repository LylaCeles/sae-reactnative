import { StyleSheet, View, Text, TouchableOpacity, Vibration } from 'react-native';
import FooterGame from './footerGame';
import React, { useState, useEffect } from 'react';
import Hasard from './Hasard';



export default function ButtonLancage({ lancageFunction = false, setCancelPopup, setChoixUser, choixUser, selectChoice, setDataDe, setUserPoint, points, userPoint, setSelectChoice, declareEndGame }) {
    const [temoin, setTemoin] = useState(1);


    function lancage() {
        lancageFunction();
        nbLancer = temoin;
        setTemoin(nb => nb + 1);


    }


    return (
        <View style={styles.contain} >
            {temoin != 3 && lancageFunction != false ?
                <TouchableOpacity style={styles.button} onPress={() => {
                    lancage();
                    Vibration.vibrate(20); 
                    
                }}>
                    <Text style={styles.buttonText}>
                        Lancer les dés
                    </Text>
                </TouchableOpacity> : <></>}

            
        </View>
    )
}

const styles = StyleSheet.create({

    contain: {
        display: "flex",
        // justifyContent:"center",
        alignItems: "center",
        flexDirection: "row",
        top: 50,
    },
    button: {
        backgroundColor: "black",
        zIndex: 500,
        padding: 5,
        margin: 5,
    },
    buttonText: {
        color: "white",
    },
});
