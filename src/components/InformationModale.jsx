import { Text, View } from "react-native";
import Modale from "./ux/Modale";
import styles from "../Style";

export default function InformationModale({ figure, condition, value, show, handleClose }) {

    return (
        <Modale handleClose={() => handleClose()} show={show}>
            <View style={styles.choiceModale}>
            <Text style={[styles.h2, styles.text]}>{figure}</Text>
            <Text style={styles.text}>{condition}</Text>
            <Text style={styles.text}>points : {value}</Text>
            </View>
        </Modale>
    )

}