import { StyleSheet, View } from "react-native";
import styles from "../../Style";
import { Pressable } from "react-native";

export default function Modale({ children, show, handleClose = false }) {
    return (
        <>
            <Pressable
                style={[styles.cancelPannel,!show && styles.hideModale, StyleSheet.absoluteFill]}
                onPress={() => { handleClose() }}>
                {/* <View style={[styles.cancelPannel, !show && styles.hideModale]}></View> */}
            </Pressable>
            <View style={[styles.Modale, !show && styles.hideModale]}>
                {/* <View styles={[styles.Modale,show==false? styles.hideModale:""]}> */}
                {children}
            </View>
        </>

    )

}