import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import React, { useState, useEffect } from 'react';
import styles from '../../Style';


export default function Dice({ value, selectRethrow, temoin, setTemoin, disabled=false }) {
    const [pressed, setPressed] = useState(false);

    useEffect(() => {
        if (temoin == false) {
            setPressed(false);
            value.relance = false;
        }
    }, [temoin]);

    function pointPattern(n){
        let pattern="";
        if(n==1){
            pattern=<View style={[styles.point, pressed==true?styles.pointSelected:""]}/>
        }
        else if(n==2){
            pattern = <View style={[styles.pointLine, styles.lineRotate]}><View style={[styles.point, pressed==true?styles.pointSelected:""]}/><View style={[styles.point, pressed==true?styles.pointSelected:""]}/></View>
        }
        else if(n==3){
            pattern = <View style={[styles.pointLine, styles.lineRotate]}><View style={[styles.point, pressed==true?styles.pointSelected:""]}/><View style={[styles.point, pressed==true?styles.pointSelected:""]}/><View style={[styles.point, pressed==true?styles.pointSelected:""]}/></View>
        }
        else if(n==4){
            pattern = <>
            <View style={styles.pointLine}><View style={[styles.point, pressed==true?styles.pointSelected:""]}/><View style={[styles.point, pressed==true?styles.pointSelected:""]}/></View>
            <View style={styles.pointLine}><View style={[styles.point, pressed==true?styles.pointSelected:""]}/><View style={[styles.point, pressed==true?styles.pointSelected:""]}/></View>
            </>
        }
        else if(n==5){
            pattern = <>
            <View style={styles.pointLine}><View style={[styles.point, pressed==true?styles.pointSelected:""]}/><View style={[styles.point, pressed==true?styles.pointSelected:""]}/></View>
            <View style={[styles.pointLine, styles.singlePointLine]}><View style={[styles.point, pressed==true?styles.pointSelected:""]}/></View>
            <View style={styles.pointLine}><View style={[styles.point, pressed==true?styles.pointSelected:""]}/><View style={[styles.point, pressed==true?styles.pointSelected:""]}/></View>
            </>
        }
        else{
            pattern = <>
            <View style={styles.pointLine}><View style={[styles.point, pressed==true?styles.pointSelected:""]}/><View style={[styles.point, pressed==true?styles.pointSelected:""]}/></View>
            <View style={styles.pointLine}><View style={[styles.point, pressed==true?styles.pointSelected:""]}/><View style={[styles.point, pressed==true?styles.pointSelected:""]}/></View>
            <View style={styles.pointLine}><View style={[styles.point, pressed==true?styles.pointSelected:""]}/><View style={[styles.point, pressed==true?styles.pointSelected:""]}/></View>
            </>
        }
        return pattern;
    }

    return (


        <TouchableOpacity
            disabled={disabled}
            style={[styles.dice, pressed==true ? styles.selectedDice :"", {justifyContent: value.value>3?"space-between":"center"}, {padding: value.value>3?4:2}]}
            onPress={() => {
                if (selectRethrow != undefined) {

                    if (pressed == true) {
                        setPressed(false);
                        // console.log(value);
                        value["relance"] = false;
                        selectRethrow(value);
                    }
                    else {
                        setTemoin(true);
                        setPressed(true);
                        // console.log(value);
                        value["relance"] = true;
                        selectRethrow(value);

                    }
                }
                // console.log("dé", value.value)

            }}>
                {pointPattern(value.value)}
            {/* <Text>
                {pressed}
                {value.value}
            </Text> */}
        </TouchableOpacity >
    )
}

