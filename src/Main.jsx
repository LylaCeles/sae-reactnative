// In App.js in a new project

import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { HomeScreen } from './screens/HomeScreen';
import { GameZoneScreen } from './screens/GameZoneScreen';
import { EndZoneScreen } from './screens/EndZoneScreen';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { LoginScreen } from './screens/LoginScreen';

const Stack = createNativeStackNavigator();

function Main() {
  return (
    <SafeAreaProvider>
      <NavigationContainer>
        <Stack.Navigator
          initialRouteName="Login"
          screenOptions={{
            headerShown: false
          }}>
            <Stack.Screen name="Login" component={LoginScreen}
            options={{
              headerStyle: {
                backgroundColor: 'blue',
              },
              headerTintColor: '#f00',
              headerTitleStyle: {
                fontWeight: 'bold',
              },
            }} 
            />
          <Stack.Screen name="Home" component={HomeScreen}
            options={{
              headerStyle: {
                backgroundColor: 'blue',
              },
              headerTintColor: '#f00',
              headerTitleStyle: {
                fontWeight: 'bold',
              },
            }} 
            />
          <Stack.Screen name="GameZone" component={GameZoneScreen}
            options={{
              headerStyle: {
                backgroundColor: '#444444',
                flex: 1,
              },
              headerTintColor: '#fff',
              headerTitleStyle: {
                fontWeight: 'bold',
              },
            }} />
          <Stack.Screen name="EndZone" component={EndZoneScreen}
            options={{
              headerStyle: {
                backgroundColor: '#444444',
                flex: 1,
              },
              headerTintColor: '#fff',
              headerTitleStyle: {
                fontWeight: 'bold',
              },
            }} />
        </Stack.Navigator>
      </NavigationContainer>
    </SafeAreaProvider>
  );
}

export default Main;