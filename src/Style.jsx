import { StyleSheet } from "react-native";


const styles = StyleSheet.create({
    // ############### Login ############

    containerLogin: {
        justifyContent: "center",
        overflow:"hidden",
        alignItems:"center",
        backgroundColor:"#A83536",
        height:"100%"
    },
    contentLogin:{
        width:"150%",
        display:"flex",
        alignItems:"center",
        backgroundColor:"#257D2F",
        paddingVertical:190,
        borderWidth:10,
        paddingHorizontal:70,
        borderRadius:500,
    },
    titleLogin:{
        color:"#F7EEB6",
    },

    // ############### Home ############


    header: {
        width: "100%",
        display: "flex",
        paddingHorizontal: "5%",
        backgroundColor: "#A83536",
        // marginBottom: 20,
    },
    footer: {
        backgroundColor: "#A83536",
        width: "100%",
        padding: 20,
        display: "block",
        marginTop: "auto",
    },
    text: {
        // color: "#207060",
        fontFamily:"Josefin Sans",
    },
    h2: {
        fontSize: 20,
        fontFamily:"Josefin Sans",
    },
    contain: {
        display: "flex",
        // justifyContent:"center",
        alignItems: "center",
        backgroundColor: "#257D2F",
        width: "100%",
        height: "100%",
    },
    // button: {
    //     display: "flex",
    //     alignItems: "center",
    //     height: "50%",
    //     width: "100%",
    // },
    button: {
        position: "absolute",
        bottom: 5,
        zIndex: 50,
        // marginTop: 10,
        borderRadius: 100,
        width: 80,
        height: 80,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: '#6E549E',

    },
    container: {
        display: "flex",
        width: "100%",
        height: "85%",
        justifyContent: "flex-start",
        alignItems: "center",
        paddingVertical: 10,
    },

    buttonText: {
        fontSize: 20,
        color: "#fff",
        fontFamily:"Josefin Sans",

    },
    buttonDisabled: {
        backgroundColor: "#aaa",
    },
    input: {
        backgroundColor: "white",
        paddingHorizontal: 20,
        paddingVertical: 5,
        width: "80%",
        border: "none",
        marginBottom: 50,
        color: "#207060",
        fontFamily:"Josefin Sans",
        borderRadius: 20,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        // borderEndEndRadius: 15,
        // borderBottomEndRadius: 15,
    },
    data: {
        width: "100%",
        height: "80%",
        display: "flex",
        paddingHorizontal: 10,
        marginBottom:10,
        // // alignItems: "center",
        // backgroundColor:"blue",
    },
    dataTitle: {
        fontSize: 12,
        fontWeight: "800",
        color: "#F7EEB6",
        marginBottom: 10,
    },
    dataText: {
        color: "#F7EEB6",
        fontFamily:"Josefin Sans",

    },
    //###################### GAMEZONE ###################

    des: {
        display: "flex",
        flexDirection: "row",
        width: "100%",
        alignItems: "center",
        justifyContent: "center",
        height: "13%",
    },
    title: {
        fontSize: 50,
        fontFamily:"Varna-Bold",
        color: "#F7EEB6",
        textAlign:"center",
    },
    nameContainer: {
        width: "80%",
        height: "auto",
    },
    pseudo: {
        fontSize: 20,
        color: "#F7EEB6",
        textAlign: "center",
        fontFamily:"Josefin Sans",
        marginLeft: 10,
    },
    buttonDice: {
        backgroundColor: "white",
        width: 40,
        borderRadius: 10,
        height: 40,
        display: "flex",
        justifyContent: "space-between",
        // alignItems:"center",
        padding: 5,
    },
    groupPoint: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "",
    },

    dice: {
        // backgroundColor:"red",
        zIndex: 56,
        backgroundColor: "#205060",
        borderRadius: 100,
        width: 10,
        height: 10,
    },
    throwButton: {
        // marginTop: 10,
        borderRadius: 10,
        paddingHorizontal: 20,
        paddingVertical: 10,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: '#6E549E',

    },
    textThrowButton: {
        color: "white",
        fontFamily:"Josefin Sans",
    },
    // nextThrow:{
    //     backgroundColor:"#D6B9ED",
    // },

    buttonContainer: {
        display: "flex",
        flexDirection: "row",
        width: "80%",
        height: 50,
        justifyContent: "center",
        gap: 20,
        // position: "absolute",
        // bottom: 20,
        zIndex: 50,
    },

    // ####### table ###############

    tableContainer: {
        backgroundColor: "white",
        width: '85%',
        // position: "relative",
        // height: "50px",
        // paddingTop: 50,
        marginBottom: 50,
    },


    tableHeader: {
        width: "85%",
        paddingHorizontal: 20,
        backgroundColor: "white",
        borderBottomColor: "#205060",
        borderBottomWidth: 1,
        // borderBlockEndColor:"#205060",
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        paddingVertical: 15,
    },
    cell: {
        flex: 1,
        alignItems: "center",
    },
    points: {
        width: "100%",
        textAlign: "right",
        fontFamily:"Josefin Sans",
    },

    // #####################POPUP GAMEZONE #######################
    cancelPannel: {
        width: "100%",
        height: "100%",
        // borderRadius:"5%",
        backgroundColor: "#205060",
        position: "absolute",
        top: 0,
        opacity: 0.5,
        zIndex: 100,
        display: "flex",
        padding: 20,
        alignItems: "center",
        justifyContent: "center",
    },
    choiceModale: {
        display: "flex",
        justifyContent: "flex-start",
        alignItems: "center",
        gap: 5,
        width: "100%",
        height: "100%",
    },

    Modale: {
        position: "absolute",
        top: 0,
        display: "flex",
        width: "80%",
        height: "50%",
        top: "25%",
        padding: 20,
        borderRadius: 15,
        backgroundColor: "white",
        zIndex: 400,
        shadowColor: '#171717',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 1,
        shadowRadius: 3,
        elevation: 5,
        justifyContent: "center",
        alignItems: "center",

    },
    hideModale: {
        display: "none",
    },
    popup: {
        backgroundColor: "white",
        // justifyContent:"center",
        padding: 5,
        borderWidth: 1,
        width: "75%",
        height: "50%",
        alignItems: "center",
    },
    containDe: {
        display: "flex",
        flexDirection: "row",
        width: "95%",
        height: "35%",
        backgroundColor: "blue",
        justifyContent: "space-between",
        alignItems: "center",

    },

    // ################ DICE #####
    dice: {
        borderRadius: 5,
        margin: 5,
        width: "10%",
        height: "45%",
        borderBlockColor: '#205060',
        borderWidth: 1,
        display: "flex",
        // padding:2,
        alignItems: "center",
        backgroundColor: "#fff"
        // justifyContent:"center",

    },
    selectedDice: {
        backgroundColor: "#205060"
    },
    pointLine: {
        width: "100%",
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
    },
    singlePointLine: {
        justifyContent: "center",
    },
    lineRotate: {
        transform: [{ rotate: '45deg' }],
    },
    point: {
        borderRadius: 100,
        width: 7,
        height: 7,
        backgroundColor: "#205060",
    },
    pointSelected: {
        backgroundColor: "#fff",
    },

    // ####### ENDZONE #######
    saveButton: {
        backgroundColor: "#D6B9ED",
        display: "flex",
        padding: 5,
        borderRadius: 5,
    },
    endMsg: {
        display: "flex",
        justifyContent: "flex-start",
        height: "85%",
    }

});


export default styles;
